#
# Billing Bot, by Olly F-G
# User signup functionality
#

from util import get_conv, set_conv, del_conv
from webhook import send_message

# Handle incoming messages
def handle(chat_id, message):
	conv = get_conv(chat_id) or {}
	if 'signup_stage' in conv:
		(None,
		signup1,
		signup2,
		signup3,
		signup4,
		signup5,
		signup6)[conv['signup_stage']](chat_id, message)
	else:
		signup1(chat_id, message)

# Starts a recipient signup process.
# Asks for the Name
def signup1(chat_id,message):
	conv = get_conv(chat_id) or {}
	conv['subject'] = 'signup'
	conv['signup_stage'] = 2
	set_conv(chat_id,conv)
	message = '''Ok, what would you like me to give your name as?'''
	send_message(None,message,chat_id=chat_id)

# Gets the name, asks for the bank name
def signup2(chat_id,message):
	text = message['text'].strip()
	if text.lower() in ('/cancel','cancel'):
		del_conv(chat_id)
		message = "Ok, bye bye!"
		send_message(None,message,chat_id=chat_id)
		return
	conv = get_conv(chat_id) or {}
	conv['signup_stage'] = 3
	conv['signup_name']=text
	set_conv(chat_id,conv)
	message = '''Good, now what is your bank's name?'''
	send_message(None,message,chat_id=chat_id)

# Gets the bank name, asks for the bank number
def signup3(chat_id,message):
	text = message['text'].strip()
	if text.lower() in ('/cancel','cancel'):
		del_conv(chat_id)
		message = "Ok, bye bye!"
		send_message(None,message,chat_id=chat_id)
		return
	conv = get_conv(chat_id) or {}
	conv['signup_stage'] = 4
	conv['signup_bname']=text
	set_conv(chat_id,conv)
	message = '''Cool, now what is the bank number you would like to be paid into?'''
	send_message(None,message,chat_id=chat_id)

# Gets the bank number, asks for the address
def signup4(chat_id,message):
	text = message['text'].strip()
	if text.lower() in ('/cancel','cancel'):
		del_conv(chat_id)
		message = "Ok, bye bye!"
		send_message(None,message,chat_id=chat_id)
		return
	conv = get_conv(chat_id) or {}
	conv['signup_stage'] = 5
	conv['signup_bnum']=text
	set_conv(chat_id,conv)
	message = '''Right, what is your postal (billing) address?'''
	send_message(None,message,chat_id=chat_id)

# Gets the address, ask for the email
def signup5(chat_id,message):
	text = message['text'].strip()
	if text.lower() in ('/cancel','cancel'):
		del_conv(chat_id)
		message = "Ok, bye bye!"
		send_message(None,message,chat_id=chat_id)
		return
	conv = get_conv(chat_id) or {}
	conv['signup_stage'] = 6
	conv['signup_addr']=text
	set_conv(chat_id,conv)
	message = '''Right, last question, what is your email address?'''
	send_message(None,message,chat_id=chat_id)

# Get the email and make the new recip
def signup6(chat_id,message):
	text = message['text'].lower().strip()
	if text in ('/cancel','cancel'):
		del_conv(chat_id)
		message = "Ok, bye bye!"
		send_message(None,message,chat_id=chat_id)
		return

	email = text.split(' ')[0]
	domain = email.split('@')[1]
	if domain != 'ollyfg.com':
		message = '''Sorry, at the moment my services are limited to ollyfg.com domains.

		This is to prevent abuse of my services. If you have a ollyfg.com email address, please tell me that.

		If you would like to have your email added to the whitelist, please email me at billbot@ollyfg.com and convince me that you're human.

		Otherwise, just say /cancel to stop signing up.
		'''
		send_message(None,message,chat_id=chat_id)
	else:
		from models import Recip
		conv = get_conv(chat_id) or {}
		recip = Recip(name=conv['signup_name'],bank_name=conv['signup_bname'],bank_number=conv['signup_bnum'],addr=conv['signup_addr'],email=email,id=chat_id)
		recip.jobs = []
		recip.put()

		del conv['signup_stage']
		del conv['subject']
		set_conv(chat_id,conv)
		message = '''Great! You're registered.

		You can now add or remove **jobs** using my /add or /remove commands.'''
		keyboard = {
			'resize_keyboard':True,
			'one_time_keyboard':True,
			'keyboard':[['/add','/remove']]
		}
		send_message(recip,message,keyboard=keyboard)
