#
# Billing Bot, by Olly F-G
# Removing a job functionality
#

from util import get_conv, set_conv
from webhook import send_message

# Handle incoming messages
def handle(chat_id, message):
	conv = get_conv(chat_id) or {}

	if 'confirming' in conv:
		remove_confirm(chat_id, message)
	else:
		remove_begin(chat_id, message)

# Ask the user which job they want to remove
def remove_begin(chat_id, message):
	from models import Recip
	recip = Recip.get_by_id(chat_id)
	if not len(recip.jobs or []):
		message = '''You don't have any recurring invoices set up yet! Add one using /add.'''
		send_message(recip,message)
		return

	conv = get_conv(chat_id) or {}
	conv['confirming'] = True
	conv['subject'] = 'remove_job'
	set_conv(chat_id,conv)
	message = '''Which recurring invoice do you want to remove?'''
	keyboard = {
		'resize_keyboard':True,
		'one_time_keyboard':True,
		'keyboard':[]
	}
	for job in recip.jobs:
		keyboard['keyboard'].append(
		['{job_name} (billed to {job_email})'.format(job_name=job.name, job_email=job.email)]
		)
	send_message(recip,message,keyboard=keyboard)

# Remove the given job
def remove_confirm(chat_id, message):
	from models import Recip
	recip = Recip.get_by_id(chat_id)
	jobs = [j.name.lower() for j in recip.jobs]
	text = message['text'].lower().strip().split(' ')

	if text[0] in ('/cancel','cancel'):
		conv = get_conv(chat_id)
		del conv['subject']
		del conv['confirming']
		set_conv(chat_id,conv)
		message = "Ok then."
		send_message(recip,message)
		return

	job_name = []
	for w in text:
		job_name.append(w)
		if ' '.join(job_name) in jobs:
			job_index = jobs.index(' '.join(job_name))
			break
	else:
		try:
			job_index = int(text[0])-1
			assert job_index<len(jobs)
			job_name = jobs[job_index]
		except:
			message = '''Sorry, that didn't look like the name of any of your recurring invoices. Please make sure you spell it correctly. Alternatively, you can just give me the index of the job to remove.'''
			send_message(recip,message)
			return
	del recip.jobs[job_index]
	recip.put()
	conv = get_conv(chat_id)
	del conv['confirming']
	del conv['subject']
	set_conv(chat_id,conv)

	message = '''Pow! {job_name} has been removed.

	You now have {number} recurring invoice{s} set up.'''.format(job_name=' '.join(job_name).title(),number=len(recip.jobs), s='s' if len(recip.jobs)!=1 else '')
	keyboard = {
		'resize_keyboard':True,
		'one_time_keyboard':True,
		'keyboard':[['/add','/remove']]
	}
	send_message(recip,message,keyboard)
	return
