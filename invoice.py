#
# Billing Bot, by Olly F-G
# Invoice creation and sending functionality
#

# Send an email invoice
def send_invoice(user, job, billables):
	import logging
	reply_to=user.email
	tos=job.email.split(';')
	if user.email not in tos:
		tos.append(user.email)
	to = tos[0]
	cc = tos[1:]
	sender=user.email
	subject='Invoice %04d'%job.index
	html=make_invoice(user, job, billables)

	logging.info("Sending email to %s"%tos)

	if not sender or not html:
		logging.error('sendEmail() with bad params (sender:%s html:%s)'%(sender, html))
		return # Not an error so task doesn't repeat

	from google.appengine.api import mail
	message=mail.EmailMessage()
	message.sender=sender
	if reply_to:
		message.reply_to=reply_to
	if to:
		message.to=to
	if cc:
		message.cc=cc
	message.subject=subject
	if html:
		message.html=html
	try:
		message.send()
	# DeadlineExceededError inherits from BaseException instead of Exception
	# and is not caught for reasons that are beyond me. So we catch BaseException
	# and check the exception name instead.
	except BaseException as e:
		if type(e).__name__ in ('DeadlineExceededError', 'ApplicationError'):
			logging.error('Email to %s failed (%s)'%(to, e))
			return
		else:
			raise	# Should keep retrying until sent.

	logging.info('Email to:%s cc:%s successful (%s)'%(to, cc, subject))

def make_invoice(sender,job,billables):

	# Make the date strings
	import time
	date = time.strftime('%-d %b %Y',time.gmtime())
	ldate = time.strftime('%b %Y',time.gmtime(time.time()-3000000))

	# Split up the addresses. We chop off the last lines if there are more than 4
	saddrs = sender.addr.split('\n')
	while len(saddrs)<4:
		saddrs.append('&nbsp;')
	jaddrs = job.addr.split('\n')
	while len(jaddrs)<4:
		jaddrs.append('&nbsp;')


	base_string='''
<table style="font-size:13px" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>Date:</td>
		<td>&nbsp;{date}</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td valign="top">To:</td>
		<td>{jaddr[0]}</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>{jaddr[1]}</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>{jaddr[2]}</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>{jaddr[3]}</td>
	</tr>
	<tr>
		<td>&nbsp;</td><td><br></td>
	</tr>
	<tr>
		<td>Attention:&nbsp;</td>
		<td>{pname}</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2"><b>Invoice Number: {index:04d}</b><br></td>
	</tr>
</table>
<table style="font-size:13px" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td valign="top">From:&nbsp;</td>
		<td>{sender}</td>
	</tr>
	<tr>
		<td><br></td>
		<td><br>{saddr[0]}</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>{saddr[1]}</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>{saddr[2]}</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>{saddr[3]}</td>
	</tr>
</table>
<br style="font-size:13px">
<table style="font-size:13px" border="0" cellpadding="2pt" cellspacing="0"><tbody>
	<tr>
		<td style="border-bottom-width:1pt;border-bottom-style:solid;border-bottom-color:black">Date</td>
		<td style="border-bottom-width:1pt;border-bottom-style:solid;border-bottom-color:black">Item</td>
		<td style="border-bottom-width:1pt;border-bottom-style:solid;border-bottom-color:black">Units</td>
		<td style="border-bottom-width:1pt;border-bottom-style:solid;border-bottom-color:black" align="right">Amount</td>
	</tr>
	'''.format(pname=job.billing_name,
				sender=sender.name,
				index=job.index,
				date=date,
				saddr=saddrs,
				jaddr=jaddrs
				)
	total = 0
	for billable in billables:

		base_string += '''
		<tr valign="top">
			<td nowrap="">{ldate}</td><td>{desc}</td>
			<td>&nbsp;{number} {unit}</td>
			<td align="right">${cost:.2f}<br></td>
		</tr>
		'''.format(ldate=ldate,
					desc=billable.description,
					number=billable.number,
					unit=billable.unit,
					cost=round(float(billable.number*billable.rate/100),2)
					)
		total += billable.number*billable.rate
	base_string+='''
	<tr>
		<td>&nbsp;</td>
		<td colspan="2"><br></td>
		<td align="right"><br></td>
	</tr>
	<tr>
		<td style="border-top-width:1pt;border-top-style:solid;border-top-color:black">&nbsp;</td>
		<td colspan="2" style="border-top-width:1pt;border-top-style:solid;border-top-color:black"><b>Invoice&nbsp;Total</b></td>
		<td style="border-top-width:1pt;border-top-style:solid;border-top-color:black" align="right"><b>${total:.2f}</b><br></td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4">For telephone/internet payments please use {bname} account&nbsp;{account}&nbsp;<wbr>quoting the&nbsp;invoice&nbsp;number. To ensure unbroken service, please pay within 7 days of&nbsp;invoice date.</td>
	</tr>
</table>'''.format(total=round(total/100.0,2),bname=sender.bank_name,account=sender.bank_number)
	return base_string
