#
# Billing Bot, by Olly F-G
# Adding a job functionality
#

from util import get_conv, set_conv
from webhook import send_message
import logging

# Handle incoming messages
def handle(chat_id, message):
	conv = get_conv(chat_id) or {}
	text = message['text'].strip()
	logging.debug("Handling an add_job request")
	if text.lower() in ('/cancel','cancel'):
		if 'billable_stage' in conv:
			del conv['billable_stage']
			del conv['job_defaults'][-1]
		else:
			del conv['add_stage']
			del conv['subject']
			for job_info in ('job_name','job_email','job_bname','job_addr','job_defaults','job_index'):
				if job_info in conv:
					del conv[job_info]
		set_conv(chat_id,conv)
		message = "Ok then."
		send_message(None,message,chat_id=chat_id)
		return

	if 'add_stage' in conv:
		logging.debug('Going to stage %d'%conv['add_stage'])
		(None,
		add1,
		add2,
		add3,
		add4,
		add5,
		add6,
		add7,
		add8,
		add9)[conv['add_stage']](chat_id, message)
	else:
		logging.debug("Starting at the start")
		add1(chat_id, message)

# Starts the process of adding a new job
def add1(chat_id, message):
	conv = get_conv(chat_id) or {}
	conv['add_stage'] = 2
	conv['subject'] = 'add_job'
	set_conv(chat_id,conv)
	message = '''Right, what is the name of the company or person you're billing?'''
	send_message(None,message,chat_id=chat_id)

# Gets the new job name, and asks for the job email
def add2(chat_id, message):
	text = message['text'].strip()
	conv = get_conv(chat_id) or {}
	conv['job_name'] = text
	conv['add_stage'] = 3
	set_conv(chat_id,conv)
	message = '''Got it, now what is the email of the company or person you're billing?'''
	send_message(None,message,chat_id=chat_id)

# Gets the new job email, and asks for the billers name
def add3(chat_id, message):
	text = message['text'].strip().lower().split()
	email = None
	for w in text:
		if '@' in w:
			email = w
			break
			if not email:
				message = '''Uh oh! I missed the email in there! I was expecting the email adress of the person/company that you want to send invoices too.'''
				send_message(None,message,chat_id=chat_id)
				return

	conv = get_conv(chat_id) or {}
	conv['job_email'] = email
	conv['add_stage'] = 4
	set_conv(chat_id,conv)
	message = '''Ok, What is the name of the person you're billing?'''
	send_message(None,message,chat_id=chat_id)

# Gets the new job biller name, and asks for the address
def add4(chat_id, message):
	text = message['text'].strip()
	conv = get_conv(chat_id) or {}
	conv['job_bname'] = text
	conv['add_stage'] = 5
	set_conv(chat_id,conv)
	message = '''Right, now what is the address of the company/person you're billing? (up to 4 lines)'''
	send_message(None,message,chat_id=chat_id)

# Gets the new address, and asks for the index
def add5(chat_id, message):
	text = message['text'].strip()
	conv = get_conv(chat_id) or {}
	conv['job_addr'] = text
	conv['add_stage'] = 6
	set_conv(chat_id,conv)
	message = '''Cool, now what invoice number are we up to? (How many invoices have you sent before).'''
	send_message(None,message,chat_id=chat_id)

# Gets the invoice index and start asking about default billables
def add6(chat_id, message):
	text = message['text'].lower().strip()
	try:
		ind = int(text.split(' ')[0])
	except:
		message = '''Oops, I didn't get that. I was expecting a number for how many invoices you've sent before.'''
		send_message(None,message,chat_id=chat_id)
		return

	conv = get_conv(chat_id) or {}
	conv['job_index'] = ind
	conv['job_defaults'] = []
	conv['add_stage'] = 7
	set_conv(chat_id,conv)
	add7(chat_id)

# Ask the user if they want to set up an(other) billable
def add7(chat_id, message=None):
	conv = get_conv(chat_id) or {}
	conv['add_stage'] = 8
	set_conv(chat_id,conv)
	nother = len(conv['job_defaults'])
	logging.debug('Defaults so far: %s'%conv['job_defaults'])
	message = '''Cool, now would you like to add a{nother} default billable?'''.format(nother='nother' if nother else '')
	send_message(None,message,chat_id=chat_id)

# Deal with adding billables
def add8(chat_id, message):
	conv = get_conv(chat_id) or {}
	text = message['text'].lower().strip()
	if 'billable_stage' not in conv:
		# User has replied y/n to "do you want to add a billable?"
		if 'yes' in text:
			from models import Billable
			conv['billable_stage'] = 1
			conv['job_defaults'].append(Billable())
			set_conv(chat_id,conv)
			message = '''Right, would you like a default name for this billable? If you do, tell me, if you don't, say no.'''
			send_message(None,message,chat_id=chat_id)
		else:
			add9(chat_id)
	else:
		stage = conv['billable_stage']
		if stage == 1:
			desc = None if text == 'no' else text
			conv['job_defaults'][-1].description = desc
			conv['billable_stage'] = 2
			set_conv(chat_id,conv)
			message = '''OK, would you like a default unit (eg. hours, credits) for this billable ({desc})? If you do, tell me, if you don't, say no.'''.format(
							desc=desc or 'unnamed')
			send_message(None,message,chat_id=chat_id)
		elif stage == 2:
			unit = None if text == 'no' else text
			conv['job_defaults'][-1].unit = unit
			conv['billable_stage'] = 3
			set_conv(chat_id,conv)
			message = '''Great, would you like a default rate for this billable ({desc})? If you do, tell me in cents/{unit}, if you don't, say no.'''.format(
							unit=unit[:-1] or '?', desc=conv['job_defaults'][-1].description or 'unnamed')
			send_message(None,message,chat_id=chat_id)
		elif stage == 3:
			try:
				rate = None if text == 'no' else float(text)
			except:
				message = '''Sorry, I was expecting a number, with no units or anything. Would you mind sending it again?'''
				send_message(None,message,chat_id=chat_id)
				return
			conv['job_defaults'][-1].rate = rate
			conv['billable_stage'] = 4
			set_conv(chat_id,conv)
			message = '''Right, would you like a default number of {unit} for this billable ({desc})? If you do, tell me, if you don't, say no.'''.format(
							unit=conv['job_defaults'][-1].unit or 'thing', desc=conv['job_defaults'][-1].description or 'unnamed')
			send_message(None,message,chat_id=chat_id)
		elif stage == 4:
			try:
				number = None if text == 'no' else int(text)
			except:
				message = '''Sorry, I was expecting a whole number, with no units or anything. Would you mind sending it again?'''
				send_message(None,message,chat_id=chat_id)
				return
			conv['job_defaults'][-1].number = number
			del conv['billable_stage']
			set_conv(chat_id,conv)
			add7(chat_id)

# Finally adds the new job to the user
def add9(chat_id, message=None):
	from models import Recip,Job
	recip = Recip.get_by_id(chat_id)
	if recip.jobs == None:
		recip.jobs = []

	conv = get_conv(chat_id) or {}
	job = Job(conv['job_name'],conv['job_bname'],conv['job_addr'],conv['job_email'],conv['job_index'],conv['job_defaults'])
	recip.jobs.append(job)
	recip.put()


	del conv['subject']
	del conv['add_stage']
	del conv['job_name']
	del conv['job_bname']
	del conv['job_addr']
	del conv['job_index']
	del conv['job_defaults']
	del conv['job_email']
	set_conv(chat_id,conv)
	reply = '''Great, {job_name} is all set up.

	You now have {number} recurring invoice{s} set up.'''.format(job_name=job.name,number=len(recip.jobs), s='s' if len(recip.jobs)!=1 else '')
	keyboard = {
		'resize_keyboard':True,
		'one_time_keyboard':True,
		'keyboard':[['/add','/remove']]
	}
	send_message(recip,reply,keyboard=keyboard)
