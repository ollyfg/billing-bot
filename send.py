#
# Billing Bot, by Olly F-G
# Sending an invoice conversation functionality
#

from util import get_conv, set_conv
from webhook import send_message
from invoice import send_invoice

# Handle incoming messages
def handle(chat_id, message):
	conv = get_conv(chat_id) or {}
	if 'send_stage' not in conv:
		send1(chat_id, message)
	else:
		(None,send1,send2,send3)[conv['send_stage']](chat_id, message)


# Start the send flow
def send1(chat_id, message):
	import logging
	logging.info("Doing send1 for chat id %s"%chat_id)
	from models import Recip
	recip = Recip.get_by_id(chat_id)
	if not len(recip.jobs):
		logging.info("Can't do billing for {}, as they have no jobs setup.".format(recip.name))
		return
	conv = get_conv(chat_id) or {}
	logging.debug("Conv(start): %s"%conv)
	conv['send_stage'] = 2
	conv['subject'] = 'send'
	if 'curr_job' not in conv:
		conv['curr_job'] = 0
		set_conv(chat_id,conv)

	if conv['curr_job'] >= len(recip.jobs):
		del conv['send_stage']
		del conv['curr_job']
		del conv['subject']
		if 'send_hours' in conv:
			del conv['send_hours']
		set_conv(chat_id,conv)
		message = "Great, that's your invoicing all wrapped up!"
		send_message(recip,message)
		logging.debug("Finished invoicing")
		logging.debug("Conv(end): %s"%conv)
		return

	if not recip.jobs[conv['curr_job']].defaults:
		send3(chat_id, message)
		return

	send2(chat_id, message)
	logging.debug("Conv(end): %s"%conv)

# Get answer from before
def send2(chat_id, message):
	import logging
	from models import Recip
	from util import question_for
	logging.debug("Entering Send2")
	text = message['text'].lower().strip() if (message and message['text'].lower().strip()!='/send') else None
	recip = Recip.get_by_id(chat_id)
	conv = get_conv(chat_id) or {}
	logging.debug("Conv: %s"%conv)
	logging.debug("message: %s"%message)

	if not text and 'billable_num' not in conv:
		logging.debug("Starting billing")
		# Starting billing for this job
		if conv['curr_job'] == 0:
			message = '''Hi {name}!'''.format(name=recip.fname())
		else:
			message = '''Ok, onto the next one!'''

		conv['billable_num'] = 0
		conv['billables'] = recip.jobs[conv['curr_job']].defaults
		logging.debug('Billables: %s'%conv['billables'])
		conv['billable_field'] = conv['billables'][conv['billable_num']].next_missing()
		set_conv(chat_id,conv)
		question=question_for(conv['billable_field']).format(
														unit=(conv['billables'][conv['billable_num']].unit or ' ')[:-1],
														units=(conv['billables'][conv['billable_num']].unit or ' ')
														)
		message += '''

			I'm doing the billing for {job.name}.

			Tell me, {question}
			'''.format(job=recip.jobs[conv['curr_job']], question=question)

	else:
		# Getting an update on the billables
		val = text
		if conv['billable_field'] == 'number' or conv['billable_field'] == 'rate':
			try:
				val = float(text.split()[0])
			except:
				message = 'Sorry, I was expecting a number please.'
				send_message(None,message,chat_id=chat_id)
				return

		if conv['billable_field'] == 'number':
			conv['billables'][conv['billable_num']].number = val
		elif conv['billable_field'] == 'rate':
			conv['billables'][conv['billable_num']].rate = val
		elif conv['billable_field'] == 'desc':
			conv['billables'][conv['billable_num']].description = val
		elif conv['billable_field'] == 'unit':
			conv['billables'][conv['billable_num']].unit = val

		# Get the next thing we need an update for
		next_field = conv['billables'][conv['billable_num']].next_missing()
		while not next_field:
			conv['billable_num'] += 1
			if conv['billable_num'] >= len(conv['billables']):
				conv['send_stage'] = 3
				set_conv(chat_id,conv)
				message = "Great, and were there any other billables?"
				keyboard = {
					'resize_keyboard':True,
					'one_time_keyboard':True,
					'keyboard':[['Yes','No']]
				}
				send_message(recip,message,keyboard=keyboard)
				return
			next_field = conv['billables'][conv['billable_num']].next_missing()
		conv['billable_field'] = next_field
		set_conv(chat_id,conv)
		question=question_for(conv['billable_field']).format(
														unit=(conv['billables'][conv['billable_num']].unit or ' ')[:-1],
														units=(conv['billables'][conv['billable_num']].unit or ' ')
														)
		message = '''Ok, {question}'''.format(question=question)

	send_message(recip,message)
	logging.debug("Conv(end): %s"%conv)



# Get if there are to be any extra billables.
def send3(chat_id, message):
	import logging
	logging.debug("Entering send3")
	text = message['text'].lower().strip()
	conv = get_conv(chat_id) or {}
	logging.debug("Conv(start): %s"%conv)
	if 'yes' in text:
		from util import question_for
		from models import Billable
		conv['billables'].append(Billable())
		conv['billable_field'] = 'desc'
		conv['send_stage'] = 2
		set_conv(chat_id,conv)
		message = '''Ok, {question}'''.format(question=question_for('desc'))
		send_message(None,message,chat_id=chat_id)
		logging.debug("Exiting expecting billable info. Conv(end): %s"%conv)
		return
	else:
		# Got all billable info, send out an email!
		from models import Recip
		recip = Recip.get_by_id(chat_id)
		job = recip.jobs[conv['curr_job']]
		total = sum(map(lambda b: b.rate*b.number, conv['billables']))
		if total:
			send_invoice(recip,job,conv['billables'])
			message = "Sent an invoice of ${total:.2f} to the fine folks at {job.name}.".format(job=job, total=total/100)
			conv['curr_job'] += 1
			conv['send_stage'] = 1
			set_conv(chat_id,conv)
			job.index+=1
			recip.put()
			logging.debug("Sent invoice")
		else:
			message = "Skipping billing for {job.name}, since you have an invoice of $0.".format(job=job)
			logging.debug("Skipping Billing")
		send_message(recip,message)
		send1(chat_id,{})
		logging.debug("Exiting send3. Conv(end): %s"%conv)
