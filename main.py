#!/usr/bin/env python
# Billing Bot, by Olly F-G
#
import webapp2

app = webapp2.WSGIApplication([
    ('/webhook',    'webhook.Main')
], debug=False)
