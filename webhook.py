#
# Billing Bot, by Olly F-G
# Main bot functionality
#
from util import BillHandler

bot_token = "269182772:AAHkulfAVMCHSi-l558IZBbhLkAlyFeaY_4"
base_url = "https://api.telegram.org/bot%s/"%bot_token

class Main(BillHandler):
	def get(self):
		return self.error(404) # Don't give away that this service exists
	def post(self):
		import json
		from util import get_conv
		body=self.request.body

		details = json.loads(body)
		self.debug(str(details))
		if 'message' not in details:
			return
		if 'text' not in details['message']:
			return
		message = details['message']
		chat_id = str(message['chat']['id'])
		text = message['text'].lower().strip()

		# See if this person exists
		from models import Recip
		r = Recip.get_by_id(chat_id)

		# First up, is this a command?
		from send import handle as handle_send
		from unregister import handle as handle_unregister
		from remove_job import handle as handle_remove_job
		from add_job import handle as handle_add_job
		from signup import handle as handle_signup
		commands = {
			'/signup':handle_signup,
			'/unregister':handle_unregister,
			'/add':handle_add_job,
			'/remove':handle_remove_job,
			'/send':handle_send,
			'/delete_conversation':delete_conversation
		}
		if text.split(' ')[0] in commands:
			commands[text.split(' ')[0]](chat_id, message)
			return
		# Are we expecting a reply?
		conv = get_conv(chat_id) or {}
		if 'subject' in conv:
			subject = conv['subject']

			if subject == 'send':
				handle_send(chat_id, message)
			elif subject == 'signup':
				handle_signup(chat_id, message)
			elif subject == 'unregister':
				handle_unregister(chat_id, message)
			elif subject == 'add_job':
				handle_add_job(chat_id, message)
			elif subject == 'remove_job':
				handle_remove_job(chat_id, message)
			else:
				import logging
				logging.warning("Got request for an unknown subject - ignoring")

		elif r:
			message = '''Hi {name}!

			You currently have *{number}* recurring invoice{s} being sent out.

			I will get in touch with you on the first of each month and work out how much to bill.'''.format(name=r.fname(),number=len(r.jobs or []), s='s' if len(r.jobs or [])!=1 else '')
			keyboard = {
				'resize_keyboard':True,
				'one_time_keyboard':True,
				'keyboard':[['/add','/remove']]
			}
			send_message(r,message,keyboard)
		else:
			message = '''Hi there!

			I send out monthly invoices on your behalf.

			Do you want to sign up with me?'''
			keyboard = {
				'resize_keyboard':True,
				'one_time_keyboard':True,
				'keyboard':[['/signup']]
			}
			send_message(None,message,keyboard=keyboard,chat_id=chat_id)

# Send a tg message
def send_message(recip, text, keyboard=None, chat_id=None):

	# Make the requests
	import json, logging
	from google.appengine.api import urlfetch

	if recip:
		chat_id = recip.key.id()

	if not chat_id:
		logging.debug("send_message called without a chat id, aborting.")
		return

	headers = {'Content-Type': 'application/json'}
	url=base_url+'sendMessage'


	payload = {
		'chat_id':chat_id,
		'text': text.replace(r'	',''),
		'parse_mode':'Markdown',
	}

	if keyboard:
		payload['reply_markup'] = keyboard


	try:
		response=urlfetch.fetch(url=url,
								payload=json.dumps(payload),
								method=urlfetch.POST,
								deadline=30,
								headers=headers)
	except BaseException, e:
		import traceback, logging
		logging.debug(traceback.format_exc())
		logging.error('Telegram sendMessage failed (deadline exceeded) - %s'%e)
	if response.status_code!=200:
		import logging
		logging.warning("Response:"+str(response.content))
		logging.error('Telegram sendMessage returned status %d when posting: %s'%(response.status_code,text))
	return

def delete_conversation(chat_id, message):
	from util import del_conv
	del_conv(chat_id)
	send_message(None, 'All done!', chat_id=chat_id)
