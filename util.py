# -*- coding: utf-8 -*-
# Billing Bot, by Olly F-G
import webapp2, logging, os

# Request handling - a very basic wrapper that lest us log messages
class BillHandler(webapp2.RequestHandler):
	def __init__(self, request=None, response=None, **kwds):
		self.initialize(request, response)
		self.log = logging.getLogger()
		self.dev = os.environ['SERVER_SOFTWARE'].startswith('Development')
		if request is not None:
			request.charset='utf-8'
			response.headers.add_header('Pragma', 'no-cache') # HTTP 1.0

	#------------------------------------------------------------------
	def debug(self, message):
		self.log.debug(u'*** ' + unicode( message ) )


# Go through all our recipients and start them all on the flow to send their invoices.
# Triggered by cron on the 1st of each month, or else when people do a manual send
class DoInvoices(BillHandler):
	def get(self):
		from models import Recip
		from webhook import send1
		self.debug('Doing invoices')
		rs = Recip.query().fetch(keys_only=True)
		for r in rs:
			self.debug("Messaging {}".format(r.id()))
			send1(r.id(),{})


# Get,set and delete memcache conversations
def get_conv(chat_id):
	from google.appengine.api import memcache
	return memcache.get(chat_id)
def set_conv(chat_id, conv):
	from google.appengine.api import memcache
	memcache.set(chat_id, conv)
def del_conv(chat_id):
	from google.appengine.api import memcache
	return memcache.delete(chat_id)

# Return a question asking for a peice of billing info
def question_for(field):
	return {
	'description':'what are you billing for?',
	'unit':'in what units are you billing? (eg. credits, hours, etc.)',
	'number':'how many {units} are you billing for?',
	'rate':'what is your billing rate in cents/{unit}?'
		}[field]
