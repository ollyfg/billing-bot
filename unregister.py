#
# Billing Bot, by Olly F-G
# Unregistering a user functionality
#

from util import get_conv, set_conv, del_conv
from webhook import send_message

# Handle incoming messages
def handle(chat_id, message):
	conv = get_conv(chat_id) or {}
	if 'confirming' in conv:
		unregister_confirm(chat_id, message)
	else:
		unregister_begin(chat_id, message)

# Ask the user to confirm unregistration
def unregister_begin(chat_id, message):
	conv = get_conv(chat_id) or {}
	conv['subject'] = 'unregister'
	conv['confirming'] = True
	set_conv(chat_id,conv)
	message = '''Are you sure you want to unregister? I will delete all your information.'''
	keyboard = {
		'resize_keyboard':True,
		'one_time_keyboard':True,
		'keyboard':[['Yes','No']]
	}
	send_message(None,message,chat_id=chat_id,keyboard=keyboard)

# Possibly remove the user
def unregister_confirm(chat_id, message):
	conv = get_conv(chat_id) or {}
	del conv['confirming']
	del conv['subject']
	set_conv(chat_id,conv)
	text = message['text'].lower().strip()
	if 'yes' in text:
		from models import Recip
		recip = Recip.get_by_id(chat_id)
		recip.key.delete()
		del_conv(chat_id)
		message = "All done! Let me know if you want to sign up again."
		send_message(None,message,chat_id=chat_id)
	else:
		message = '''Ok, well just let me know if you ever do want to unregister.'''
		send_message(None,message,chat_id=chat_id)
