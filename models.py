# -*- coding: utf-8 -*-
# Billing Bot, by Olly F-G

from google.appengine.ext import ndb

# A Recipient. The id is the telegram chat id
class Recip(ndb.Model):
	name = ndb.StringProperty(required=True, indexed=False)
	email = ndb.StringProperty(required=False, indexed=True)
	jobs = ndb.PickleProperty()	# A list of jobs this recip has set up
	bank_number = ndb.StringProperty(indexed=False)
	bank_name = ndb.StringProperty(indexed=False)
	addr = ndb.StringProperty(indexed=False)
	def fname(self):
		return self.name.split(' ')[0]

# A job object, or something that needs regular billing
class Job(object):
	# Make a new job
	def __init__(self,name,bname,addr,email,index,defaults):
		self.name = name			# Company name
		self.billing_name = bname	# Billing officer name
		self.addr = addr			# Billing office address
		self.email = email			# Company email (to send invoice too)
		self.defaults = defaults	# Default things to be billed
		self.index = index			# Which number invoice this is

# A billable 'thing'
class Billable(object):
	# Create a new billable
	def __init__(self, description=None, unit=None, rate=None, number=None):
		self.description = description	# What this billable is
		self.unit = unit				# What units this billable is in
		self.rate = rate				# Cost in cents/unit for this billable
		self.number = number			# How many units have been used
	# Get the next bit of this billable that needs to be filled in
	def next_missing(self):
		if self.description == None:
			return 'description'
		elif self.unit == None:
			return 'unit'
		elif self.rate == None:
			return 'rate'
		elif self.number == None:
			return 'number'
		return None
